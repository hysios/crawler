


build:
	@-rm crawler.zip
	@zip -r crawler.zip . -x .git/\* ansible/\* node_modules/\* data/\* example.png

deploy: build
	@ansible-playbook -i ansible/hosts ansible/site.yml
