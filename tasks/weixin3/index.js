const Promise = require('bluebird');
const path = require('path');
var {sleep} = require(path.join(process.cwd(), './lib/utils'));
var {Restart} = require(path.join(process.cwd(), './lib/status'));
// var injectScript = path.join(process.cwd(), './client/dist/index.js')
// var bundleFile = require('./bundle');
// var watch = require('watch');

module.exports = async function(page, worker) {

	return new Promise(async (resolve, reject) => {
		var exit = false;
		let wait = async function() {
			return await page.evaluate(() => {
				return new Promise((resolve) => {
					setTimeout(resolve, 100000000000);
				});
			});	
		}
		console.log('starting...')

		let startup = async function() {
			let startpage = worker.startupPage || 'https://wx.qq.com';
			await page.goto(startpage, {waitUntil: 'networkidle'})
			try {
				// let cookies = worker.readJSON('cookies.json');
				// await page.setCookies(cookies);
			} catch(err) {
				//
			}			
			let randX = Math.ceil(Math.random() * 200), randY = Math.ceil(Math.random() * 100)
	    	await page.setViewport({width: 800 + randX, height: 600 + randY})
	    	console.log(await page.viewport());		
			await page.on('framenavigated', async (frame) => {
				// await page.injectFile(injectScript);
				let result = await wait();
				console.log(result)
			});

			// await page.injectFile(injectScript);
			await page.on('pageerror', async (err) => {
				console.error('PAGEERROR', err)
			});
			await page.on('error', async (err) => {
				console.error('ERROR', err)
				page = await worker.newPage();
		  		startup();
			});	
		    await page.on('dialog', async dialog => {
		      console.log('Dialog', dialog.message());
		      await dialog.accept();
		      // await browser.close();
		    });

		    await page.on('load', async ()=> {
			    await page.evaluate(function() {
			    	setTimeout(function() {
			    		console.log('reload');
			    		location.reload();
			    	}, 600000)	
			    });		    	
		    })

		    worker.browser._connection._ws.on('close', function() {
				console.log('browser close');
		    	reject(Restart);
		    });
		}

		await startup();

		while(!exit) {
			try {
				await wait();
				console.log('step');
			} catch(e) {
				sleep(100);
				// console.error(e)
			}
		}
	})
}