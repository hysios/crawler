const Promise = require('bluebird');

module.exports = async function(page, worker) {
	let wait = async function() {
		return await page.evaluate(() => {
			return new Promise((resolve) => {
				setTimeout(resolve, 100000000000);
			});
		});	
	}

	await page.goto('http://blank.org/', {waitUntil: 'networkidle'});
	while(1) {
		await wait();
	}
}