module.exports = async function(page) {
  	await page.goto('http://blank.org/', {waitUntil: 'networkidle'});
	let result = await page.evaluate(() => {
		if (/HeadlessChrome/.test(window.navigator.userAgent)) {
		    console.log("Chrome userAgent headless detected");
		}		
	  	return Promise.resolve(true);
	});
	console.log(result); // prints "56"  return await page.title()

	result = await page.evaluate(() => {
		if(navigator.plugins.length == 0) {
		    console.log("It may be Chrome plugins headless");
		}
	  	return Promise.resolve(true);
	});	

	result = await page.evaluate(() => {
		if(navigator.languages == "") {
		    console.log("Chrome languages headless detected");
		}
	  	return Promise.resolve(true);
	});

	result = await page.evaluate(() => {
		var canvas = document.createElement('canvas');
		var gl = canvas.getContext('webgl');
		if (!gl) {
		    console.log("Chrome webgl headless detected");
		    return Promise.resolve(true);
		}

		var debugInfo = gl.getExtension('WEBGL_debug_renderer_info');
		var vendor = gl.getParameter(debugInfo.UNMASKED_VENDOR_WEBGL);
		var renderer = gl.getParameter(debugInfo.UNMASKED_RENDERER_WEBGL);

		if(vendor == "Brian Paul" && renderer == "Mesa OffScreen") {
		    console.log("Chrome webgl headless detected");
		}	  	
		return Promise.resolve(true);
	});

	result = await page.evaluate(() => {
		if (window.devicePixelRatio && devicePixelRatio >= 2) {
			var testElem = document.createElement('div');
			testElem.style.border = '.5px solid transparent';
			document.body.appendChild(testElem);
			if (testElem.offsetHeight !== 1)	{
			    console.log("Chrome hairlines headless detected");
				// document.querySelector('html').classList.add('hairlines');
			} 
			document.body.removeChild(testElem);
		}
	  	return Promise.resolve(true);
	});
	
	result = await page.evaluate(() => {
		var body = document.getElementsByTagName("body")[0];
		var image = document.createElement("img");
		image.src = "http://iloveponeydotcom32188.jg";
		image.setAttribute("id", "fakeimage");
		body.appendChild(image);
		image.onerror = function(){
		    if(image.width == 0 && image.height == 0) {
		        console.log("Chrome Image headless detected");
		    }
		}
	  	return Promise.resolve(true);
	});
}