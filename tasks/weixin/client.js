const {sleep} = require('./src/utils');
const browserify = require('browserify');
const tmp = require('tmp');
const fs = require('fs');
const path = require('path');
// const b = browserify();
const injectScript = path.join(path.join(process.cwd(), './client/dist/index.js'));

class Client {

  /**
   * connect 打开指定的登陆地址
   * @param {Page} 
   * @param  {String}
   * @return {Client}
   */
  async connect(page, url) {
    this.page = page;
    let randX = Math.ceil(Math.random() * 200), randY = Math.ceil(Math.random() * 100)
    await this.page.setViewport({width: 960 + randX, height: 718 + randY})
    console.log(await this.page.viewport());
    // console.log(this.browser.wsEndpoint());


    await sleep(1000)
    await this.page.goto('https://wx.qq.com', {waitUntil: 'networkidle'});
    this.page.on('console', (...args) => {
      for (let i =0; i < args.length; ++i)
        console.log(`worker ${i}: ${args[i]}`);
    });

    this.page.on('framenavigated', async (...args) => {
      console.log(args);
      await this.injectFile(injectScript);
      // await this.run()
    })

    this.page.on('error', (err) => {
      console.log(err)
    })
    
    this.page.on('pageerror', (err) => {
      console.log(err)
    })

    this.page.on('dialog', async dialog => {
      console.log('Dialog', dialog.message());
      await dialog.accept();
      // await browser.close();
    });

    await this.page.exposeFunction('send', (text) => {
      console.log(text);
    }); 

    await this.page.evaluate(() => {
      window.addEventListener("message", receiveMessage, false);

      function receiveMessage(event) {
        console.log(event.data)
      }
    })    
  }

  /** 
   * run 消息轮询泵机
   * @return {Promise}
   */
  async run() {
    return await this.page.waitForFunction("typeof main === 'undefined' ? void(0): main()", {
      polling: 'mutation', timeout: 1000000000
    });
  }

  async loop() {
    this._run = true;
    while (this._run) {
      await this.page.waitForFunction("typeof main === 'undefined' ? void(0): main()", {
        polling: 'mutation', timeout: 1000000000
      });
      await sleep(100);
    }
  }

  close() {
    this._run = false
  }
  /**
   * injectFile 注入代码文件
   * @param  {String}
   * @return {Promise}
   */
  async injectFile(filename) {
    // let tmpname = tmp.tmpNameSync(),
    //   buf = await this.bundleFile(filename);
    // await this._writeFile(tmpname, buf);
    await this.page.injectFile(filename);
  }

  bundleFile(filename) {
    let b = browserify();
    return new Promise((resolve) => {
      b.add(filename);
      b.bundle((err, buf) => {
        resolve(buf);
      });
    });
  }

  _writeFile(file, buf) {
    return new Promise((resolve) => {
      fs.writeFile(file, buf, resolve)
    })
  }

  /**
   * sendText 发送文本信息给制定的房间或用户
   * @param  {String} msg   
   * @param  {String} room_or_user
   * @return {Promise}
   */
  async sendText(msg, room_or_user) {

  }

  /**
   * connect 打开指定的页面
   * @param  {String} url
   * @return {Client}
   */
  static async connect(url) {
    let client = new Client();
    await client.connect(url);
    return client;
  }
}

function connectPage(page, wsUrl) {
   return Client.connect(page, wsUrl);
}

module.exports = {
  connectPage
}
