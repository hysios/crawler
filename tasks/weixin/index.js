var Client = require('./client');
var path = require('path');
var {sleep} = require('./src/utils');

module.exports = async function(page) {
	let client = await Client.connectPage(page, 'https://wx.qq.com')
	console.log('starting...')
	await client.injectFile(path.join(process.cwd(), './client/dist/index.js'))
	await client.loop();
}