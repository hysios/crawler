const Promise = require('bluebird');
const AWS = require('aws-sdk');
const path = require('path');
const util = require('util');
AWS.config.loadFromPath(path.join(__dirname, './config.json'));
let sqs = new AWS.SQS({apiVersion: '2012-11-05'});


function createQueue(queueName) {
	let params = {
		QueueName: queueName,
		Attributes: {
			'ReceiveMessageWaitTimeSeconds': '20',
		}
	};
	return new Promise((resolve, reject) => {
		sqs.createQueue(params, (err, data) => {
			if (err) {
				reject(err)
			} else {
				resolve(data.QueueUrl);
			}			
		})
	});
}

function sendMessage(url, msg) {
	let {sender, content} = msg;
	let params = {
		DelaySeconds: 3,
		MessageAttributes: {
			"SenderID": {
				DataType: "String",
				StringValue: sender.id
			},
			"TaskId": {
				DataType: "String",
				StringValue: msg.taskId
			},
		
			"MsgID": {
				DataType: "String",
				StringValue: sender.msgId
			},
			"MsgType": {
				DataType: "String",
				StringValue: sender.msgType
			},
			// "SenderName": {
			// 	DataType: "String",
			// 	StringValue: sender.name
			// },
			"SenderNickName": {
				DataType: "String",
				StringValue: sender.nickname
			},
			// "RoomID": {
			// 	DataType: "String",
			// 	StringValue: msg.roomId
			// }
		},
		MessageBody: msg.content,
		QueueUrl: url
	};
	if (msg.uin) {
		params["MessageAttributes"]["WeixinUin"] = {
			DataType: "String",
			StringValue: msg.uin
		}
	}
	
	return new Promise((resolve, reject) => {
		sqs.sendMessage(params, function(err, data) {
			if (err) {
				reject(err);
			} else {
				resolve(data.MessageId);
			}
		});	
	})
				
}

function receiveMessage(url) {
	let params = {
		AttributeNames: [
			"SentTimestamp"
		],
		MaxNumberOfMessages: 1,
		MessageAttributeNames: [
			"All"
		],
		QueueUrl: url,
		WaitTimeSeconds: 20
	};

	return new Promise((resolve, reject) => {
		sqs.receiveMessage(params, (err, data) => {
	  		if (err) {
	    		reject(err);
	  		} else {
	    		resolve(data);
		  	}
		});	
	});
}

function deleteMessage(url, message) {
	var params = {
		QueueUrl: url,
		ReceiptHandle: message.ReceiptHandle,
	};

	return new Promise((resolve, reject) => {
		sqs.deleteMessage(params, function(err, data) {
	  		if (err) {
	  			reject(err);
	  		} else {
	  			resolve(data);
	  		}
		});	
	});
}


async function messageLoop(url, handle) {
	while(1) {
		let data = await receiveMessage(url)
		if (typeof handle === 'function' ) {
			if (handle(data)) {
				for (let i in data.Messages) {
					let message = data.Messages[i];
					await deleteMessage(url, message);
				}
			}
		}
	}
}

module.exports = async function(task) {
	const queueName = `crawler-send`;
	const receQueueName = `crawler-${task.id}-rece`;

	try {
		let sqsQueueURL = await createQueue(queueName);
		let sqlReceQueueURL = await createQueue(receQueueName);

		messageLoop(sqlReceQueueURL, async (data) => {
			if (data.Messages && data.Messages.length > 0) {
				let message = data.Messages[0];
				console.log('text message', message.Body);
				await task.sendChat('测试群', message.Body)
				return true
			}
			return false
		});

		task.task.on('message', function(_msg) {
			let {msg} = _msg;
			if (msg) {
				msg.taskId = task.id;
				console.log(util.inspect(msg));
				sendMessage(sqsQueueURL, msg);
			}
		})
	} catch (err) {
		console.error(err);
	}
}