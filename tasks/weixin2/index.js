const Promise = require('bluebird');
const path = require('path');
var {sleep} = require(path.join(process.cwd(), './lib/utils'));
var {Restart} = require(path.join(process.cwd(), './lib/status'));
var injectScript = path.join(process.cwd(), './client/dist/index.js')
var bundleFile = require('./bundle');
var watch = require('watch');

module.exports = async function(page, worker) {

	return new Promise(async (resolve, reject) => {
		var exit = false;
		let wait = async function() {
			return await page.evaluate(() => {
				return new Promise((resolve) => {
					setTimeout(resolve, 100000000000);
				});
			});	
		}
		console.log('starting...')


		let startup = async function() {
			let startpage = worker.startupPage || 'https://wx.qq.com';
			await page.goto(startpage, {waitUntil: 'networkidle'})
			try {
				let cookies = worker.readJSON('cookies.json');
				await page.setCookies(cookies);
			} catch(err) {
				//
			}			
			let randX = Math.ceil(Math.random() * 200), randY = Math.ceil(Math.random() * 100)
	    	await page.setViewport({width: 800 + randX, height: 600 + randY})
	    	console.log(await page.viewport());		
			await page.on('framenavigated', async (frame) => {
				// await page.injectFile(injectScript);
				let result = await wait();
				console.log(result)
			});

			await page.on('load', async (frame) => {
				await page.injectFile(injectScript);
				let status = await page.evaluate(() => {
  					return xx.status
				});
				console.log(status);

				if (status == "running") {
					let cookies = await page.cookies();
					worker.writeJSON('cookies.json', cookies);
				}
			});

		    await page.exposeFunction('sendMessage', (msg) => {
		    	try {
		    		// console.log(_msg);
		    		// let msg = JSON.parse(_msg);
		    		process.send({msg: msg});	
		    	} catch(e) {
		    		console.error(e)
		    		//
		    	}
		    }); 

			let paths = {
				client: path.join(process.cwd(), './client'),
				src: path.join(process.cwd(), './client/src'),
				main: path.join(process.cwd(), './client/src/index.js'),
				destFile: path.join(process.cwd(), './client/dist/index.js')
			};
			watchDir = path.join(process.cwd(), './client/src');
			await watch.watchTree(paths.src, async (f, curr, prev)  => {
				if (typeof f == "object" && prev === null && curr === null) {
					await bundleFile(paths.main, paths.destFile);
				// Finished walking the tree
				} else if (prev === null) {
					await bundleFile(paths.main, paths.destFile);
				} else if (curr.nlink === 0) {
					await bundleFile(paths.main, paths.destFile);
				} else {
					await bundleFile(paths.main, paths.destFile);
				}
				await page.injectFile(injectScript);
			})	


			// await page.injectFile(injectScript);
			await page.on('pageerror', async (err) => {
				console.error(err)
				// page.close();
				// page = await worker.newPage();
				// // await page.evaluateOnNewDocument(sniffDetector);
				// startup();
			});
			await page.on('error', async (err) => {
				console.error(err)
				// page.close();
				page = await worker.newPage();
		  		// await page.evaluateOnNewDocument(sniffDetector);
		  		startup();
			});	
		    await page.on('dialog', async dialog => {
		      console.log('Dialog', dialog.message());
		      await dialog.accept();
		      // await browser.close();
		    });
		    await page.on('load', async ()=> {
			    await page.evaluate(function() {
			    	setTimeout(function() {
			    		console.log('reload');
			    		location.reload();
			    	}, 600000)	
			    });		    	
		    })
		    worker.browser._connection._ws.on('close', function() {
				console.log('browser close');
		    	reject(Restart);
		    });
		}

		await startup();

		while(!exit) {
			try {
				await wait();
				console.log('step');
			} catch(e) {
				sleep(100);
				// console.error(e)
			}
		}
	})
}