const browserify = require('browserify');
const fs = require('fs');

function bundleFile(filename, dest) {
    let b = browserify();
    return new Promise((resolve) => {
		b.add(filename);
		b.bundle((err, buf) => {
			console.log('compile to:', dest);
			fs.writeFile(dest, buf, resolve)
		});
    });
}

module.exports = bundleFile;