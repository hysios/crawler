module.exports = async function(page) {
  await page.goto('http://www.163.com', {waitUntil: 'networkidle'});
  await page.screenshot({path: '163.com.png'});
  return await page.title()
}