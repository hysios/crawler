const {getWeixin}= require('./weixin');
const {sleep} = require('./utils');
const util = require('util');
const EventEmitter = require('events');

function onMessage(msgs) {
	msgs.forEach((msg) => {
		let sender = msg['sender'];
		sendMessage(msg);
	})
}

function main() {
	let wx = getWeixin({disableObserver: false});

	if (window.pollingId) {
		clearInterval(window.pollingId)
	}

	if (window.infoId) {
		clearInterval(window.infoId)
	}

	wx.removeListener('message', onMessage);
	wx.addListener('message', onMessage);

	window.pollingId = setInterval(() => {
		// if (wx.status == 'running') {
		// 	wx.openChat('测试群');
		// }
		// console.log('status:', wx.status);
		wx.polling();
	}, 100)

	window.infoId = setInterval(() => {
		// console.log('status:', wx.status);
		let info = {
			status: wx.status,
			nickname: wx.currentNickname(),
			uin: wx.currentUin(),
			room: wx.currentRoom(),
			username: wx.currentUserName(),
			chats: wx.getChatList().length,
		}
		console.log('info', util.inspect(info))
	}, 25000)	
}

main();