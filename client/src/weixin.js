const EventEmitter = require('events');
const querystring = require('querystring');
const {sleep} = require('./utils');
const url = require('url');
const {parseMessage, parseChatroom, parseText} = require('./utils');

if (!window.msgs) {
	window.msgs = {};
}

class Weixin extends EventEmitter {

	get status() {

		// if (this.$elem.find('.header .info .display_name').length > 0) {
		// 	return "running";
		// }

		if (this.$elem.find(">.login:visible").length > 0) {
			return "login";
		}

		if (this.$elem.find(">.main:visible").length > 0) {
			return "running";
		}

		return "unknown";
	}

	get rooms() {
		if (!window.rooms) {
			window.rooms = {}
		}

		return window.rooms;
	}

	get $titleWrap () {
		return this.$chatArea.find(".box_hd>.title_wrap");
	}

	get $chatRoomMembersWrap() {
		return this.$chatArea.find(".box_hd>#chatRoomMembersWrap");
	}

	get $panel() {
		return this.$elem.find(".panel");
	}

	constructor(element, options = {}) {
		super();
		this.$elem = element;

		// this.$panel = this.$elem.find(".panel");
		// this.$contactList = this.$elem.find("");
		this.$chatArea = this.$elem.find("#chatArea");
		this.$header = this.$panel.find(">.header")
		this.$search = this.$panel.find("#search_bar")
		this.$tab = this.$panel.find(".tab");
		this.$chatList = this.$panel.find(".nav_view>.chat_list");
		this.$contactList = this.$panel.find(".nav_view>.contact_list");
		this.$readList = this.$panel.find(".nav_view>.read_list");
		// this.$input = this.$panel.find(".box-ft");
		this.$editArea = this.$panel.find("#editArea");
		this.delayBase = 200
		this.delayUpdown = 100
		this.chats = {};

		if (!options.disableObserver) {
			this.observeChats();
		}

		this.listenerRooms = (options.rooms || []);

		this.processQueue = [];
		this.on('bubble', this.onBubble.bind(this))
	}

	observeChats() {
		var obs = new MutationObserver( (mutations, observer) => {
			console.log(mutations);
			for (let i in mutations) {
				let mutation = mutations[i];
				if (mutation.type == "characterData") {
					if (mutation.target.nodeType == 3) {
						if (mutation.target.parentElement.classList.contains("web_wechat_reddot_middle")) {
							console.log("bubble:", mutation.target.nodeValue);
							this.emit('bubble', mutation.target.nodeValue, mutation.target.parentElement)
						}
					}
				} else if (mutation.type == "childList") {
					// if (mutation.target !== this.$chatList[0]) {
					// 	continue
					// }
					if (mutation.addedNodes.length > 0) {
						for (let j in mutation.addedNodes) {
							let node = mutation.addedNodes[j];
							if (node.nodeType == 1 && node.nodeName == "DIV" && node.classList.contains("ng-scope")) {
								let ng_repeat = node.attributes[0];
								if (ng_repeat.name == "ng-repeat" && ng_repeat.value == "chatContact in chatList track by chatContact.UserName") {
									console.log('add new chat')
									this.emit('chat:new', node)
									// console.log(node.attributes["ng-repeat"])
								}
							}
							// console.log('add', node.innerHTML)
						}
					}

					// if (mutation.removedNodes.length > 0) {
					// 	for (let j in mutation.removedNodes) {
					// 		let node = mutation.removedNodes[j];
					// 		console.log('remove', node.innerHTML)
					// 	}
					// }
				}
			}
        });
        obs.observe( this.$chatList[0], { childList:true, subtree:true, characterData: true });
	}

	polling() {
		if (this.status == 'running') {
			let msgs = this.getMessageList();
			this.emit('message', msgs);

			let bubbs = this.getBubbleList();
			// console.log('bubbles: '+ bubbs.length);
		}
	}

	delayTime(base = 100, updown = 50) {
		return base + Math.ceil((Math.random() * updown) - (updown / 2));
	}

	getContactList() {
		let exp = '[ng-repeat="item in allContacts"]',
		 	$contactItems = this.$contactList.find(`${exp}  .contact_item`),
		 	items = [];

		for (let i = 0; i < $contactItems.length; i++) {
			try {
				let contItem = $contactItems[i],
					u = url.parse($(contItem).find('.avatar>img').attr('mm-src'), true);

				items.push({
					elem: contItem,
					username: u.query.username,
					title: $(contItem).find('.info>h4').text()
				});
			} catch(e) {
				console.log(`getContactList error: ${e}`)
			}
		}

		return items;
	}

	getChatList() {
		let exp = '[ng-repeat="chatContact in chatList track by chatContact.UserName"]',
			$chatItems = this.$chatList.find(`${exp} .chat_item`),
			items = [];

		for (let i = 0; i < $chatItems.length; i++) {
			try {
				let chatItem = $chatItems[i],
					u = url.parse($(chatItem).find('.avatar>img').attr('mm-src'), true);

				items.push({
					elem: chatItem,
					username: u.query.username,
					title: $(chatItem).find('.info>h3>.nickname_text').text()
				});
			} catch(e) {
				console.log(`getChatList error: ${e}`)
			}
		}

		return items;
	}

 	getBubbleList() {
	    let exp = '[ng-repeat="chatContact in chatList track by chatContact.UserName"]',
	      $chatItems = this.$chatList.find(`${exp} .chat_item`),
	      items = [];

	    for (let i = 0; i < $chatItems.length; i++) {
	      try {
	        let chatItem = $chatItems[i],
						u = url.parse($(chatItem).find('.avatar>img').attr('mm-src'), true),
	          bubble_count = parseInt($(chatItem).find('.avatar>i.web_wechat_reddot_middle').text());
	        
	        if (bubble_count > 0) {
	          items.push({
	            elem: chatItem,
	            username: u.query.username,
	            title: $(chatItem).find('.info>h3>.nickname_text').text(),
	            bubble_count: bubble_count
	          });
	        }
	      } catch(e) {
	        console.log(`getBubbleList error: ${e}`)
	      }
	    }

	    return items;    
    }

    // getRoomMembers() {
    // 	if (this.$chatRoomMembersWrap.chilren().length > 0) {
    		
    // 	}
    // }
 	
 	onBubble (node, elem) {
 		let $chat_item = $(elem).parents(".chat_item"),
 			username = $chat_item.attr('data-username');
 		if (username.slice(0, 2) === "@@") { // group
			$chat_item.click();
			let msgs = this.getMessageList();
			this.emit('message', msgs);
 		} else if (username.slice(0, 1) === "@") {// private 
 			$chat_item.click();
			let msgs = this.getMessageList();
			this.emit('message', msgs);
 		} else {

 		}
 		console.log(node, elem);
 	}

	async openTab(name) {
		this.$tab.find(`.chat[ui-sref="${name}"]`).click()
		await sleep(this.delayTime())
	}

	async openContactChat(username)  {
		await this.openTab('contact')
		let items = this.getContactList();
		let elem;
		for (let i = 0; i < items.length; i++) {
			let item = items[i];
			if (item.username === username) {
				elem = item.elem
				break
			}
		}

		if (elem) {
			$(elem).click()
			await sleep(this.delayTime())
		}
	}

	async openChatById(username) {
		await this.openTab('chat')
		let items = this.getChatList();
		let elem;
		for (let i = 0; i < items.length; i++) {
			let item = items[i];
			if (item.username === username) {
				elem = item.elem
				break
			}
		}

		if (elem) {
			$(elem).click()
			await sleep(this.delayTime())
		}
	}

	async openChat(nickname) {
		await this.openTab('chat')
		let items = this.getChatList();
		let elem;
		for (let i = 0; i < items.length; i++) {
			let item = items[i];
			if (item.title === nickname) {
				elem = item.elem
				break
			}
		}

		if (elem) {
			$(elem).click()
			await sleep(this.delayTime())
		}
	}

	reload() {
		window.location.reload();
	}

	currentUserName() {
		let src = this.$header.find('.avatar>.img').attr('src');
		if (src) {
			let u = url.parse(src),
			q = querystring.parse(u.query);
			
			return q && q['username'];
		} 
		return null
	}

	currentNickname() {
		return this.$header.find('.info .display_name').text()
	}

	currentUin() {
		return this.getCookie('wxuin');
	}

	currentRoom() {
		let $title = this.$chatArea.find('.title_wrap>.title>.title_name')
		return {
			username: $title.attr('data-username'),
			title: $title.text()
		}
	}

	getCookie(name) {
		let cookies = document.cookie.split('; ');
		for (let i in cookies) {
			let cookie = cookies[i];
			if (cookie.indexOf(name+'=') === 0) {
				return cookie.split('=')[1];
			}
		}
		return null
	}

	getMessageList() {
		let room = parseChatroom(),
			$messages = this.$chatArea.find("[mm-repeat=\"message in chatContent\"] .message"),
			msgs = []
		// this.rooms[room.username] = room;
		room = this.getRoom(room.username, room);
		for (let i = 0 ;i < $messages.length; i++) {
			let message = $messages[i];

			let [msg, isNew] = parseMessage(message)
			if (msg) {
				let sender = msg['sender'];
				msg['roomId'] = room.username;
				msg['uin'] =  this.currentUin();
				if (isNew) {
					window.msgs[sender.msgId] = msg;
					if (sender['id'] != this.currentUserName()) {
			 	    	msgs.push(msg);
					}    			
				}
			}
		}

    	return msgs;
	}

	getRoom(username, room) {
		if (!this.rooms[username]) {
			this.rooms[username] = room
		}
		return this.rooms[username];
	}
}

function getWeixin(options = {}) {
	if (!window.xx) {
		window.xx = new Weixin($('body'), options);
	}

	return window.xx;
}

module.exports = {
	Weixin,
	getWeixin
}
