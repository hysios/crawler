function parseMessage(message) {
	let avatar = $(message).find('.avatar'),
		content = $(message).find('.content'),
		isMe = $(message).hasClass('me'), 
		sender, user,
		bubble = $('.bubble', content),
		bubblecm = bubble.data('cm'),
		msgId = bubblecm["msgId"];

	if (window.msgs[msgId]) {
		return [window.msgs[msgId], false]
	}

	try {
		let avatarcm = avatar.data('cm'),
			title = avatar.attr('title');
		let nickname = $('.nickname', content),
			bubble_cont = $('.bubble_cont', content);

		sender = {
			id: bubblecm["actualSender"],
			msgId: bubblecm["msgId"],
			msgType: bubblecm["msgType"],
			nickname: title
		};

		let ngif, isText, text;

		if (bubble.length == 0) {
			text = ""
		} else {
			ngif = bubble_cont.attr('ng-if');
			isText = !!~ngif.indexOf("message.MsgType == CONF.MSGTYPE_TEXT") ? true : false;
			text = isText ? parseText($('.bubble_cont>.plain>pre', content)[0]): '';
		}

		return [{
			sender,
			content: text
		}, true]

	} catch(e) {
		console.log(e)
		return [null, false];
	}
}

function parseText(pre) {
	let msg = [];
	console.log(pre.childNodes.length)
	for (let i=0; i < pre.childNodes.length; i++) {
		let node = pre.childNodes[i];
		switch(node.nodeType)	 {
			case 3:
				msg.push(node.nodeValue)
			case 1:
				if (node.tagName == "IMG") {
					let textattr = node.attributes["text"]; 
					if (textattr) {
						msg.push(textattr.value.replace(/_web$/, ''))
					}
				}

		}
	}
	return msg.join(" ")
}

function parseChatroom() {
	try {
		let title_name = $('.title_wrap .title_name');
		return {
			username: title_name.data('username'),
			title: title_name.text(),
			messages: {}
		}
	} catch(e) {
		return {}
	}
}

function lastArg(args) {
	if (args.length > 0) {
		return args[args.length-1];
	} 

	return null;
}


function sleep(ms) {
	return new Promise((resolve) => {
		setTimeout(resolve, ms);
	});
}


module.exports = {
	parseChatroom,
	parseText,
	parseMessage,
	lastArg,
	sleep
};


