const express = require('express');
const app = express();
const http = require('http').Server(app);
const bodyParser = require('body-parser');
const uuidv4 = require('uuid/v4');

const Task = require('./lib/task');
let taskBus = {};
global.taskBus = taskBus;
const WsServer = require('./lib/ws_server');
const port = 8090;

app.set('view engine', 'ejs');
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())
app.use(require('express-promise')());

app.get('/', function (req, res) {
  res.send('Hello World!')
});

app.post('/task/', function(req, res) {
	if (!req.body.name) {
	  res.status(500).send('missing valid task name!');
	  return 
	}

	let tskOpt = taskOpt(req.body);
	if (!tskOpt.id) {
		tskOpt.id = uuidv4();
	}

	let tsk = runTask(req.body.name, tskOpt)

	if (req.query.async) {
		res.send({taskId: tsk.id})
	} else {
		res.send({taskId: tsk.id, result: tsk.getResult()})
	}
});

app.get('/task', function(req, res) {
	let taks = Object.keys(taskBus).map(function (id) {
	  return taskBus[id].toJSON();
	});

	res.send(taks)
});

app.get('/task/:id', function(req, res) {
	let id = req.params.id,
		tsk = taskBus[id];
	if (!tsk) {
	  res.status(500).send('invalid task id!');
	  return
	}

	res.send(task)
});

app.get('/task/:id/screenshot', function(req, res) {
	let id = req.params.id,
		tsk = taskBus[id];
	if (!tsk) {
	  res.status(500).send('invalid task id!');
	  return
	}

	tsk.getScreenshot().then((buf) => {
		res.send({
			screenshot: `data:image/jpeg;base64, ${buf.toString('base64')}`
		});
	})
});

app.get('/task/:id/screen', function(req, res) {
	let id = req.params.id,
		tsk = taskBus[id];
	if (!tsk) {
	  res.status(500).send('invalid task id!');
	  return
	}

	// tsk.getScreenshot().then((buf) => {
	// 	res.send({
	// 		screenshot: `data:image/jpeg;base64, ${buf.toString('base64')}`
	// 	});
	// })

	res.render('screen', {taskId: id})
});

app.delete('/task/:id', function(req, res) {
	let id = req.params.id,
		tsk = taskBus[id];
	if (!tsk) {
	  res.status(500).send('invalid task id!');
	  return
	}

	delete taskBus[id];
	tsk.close();
	console.log('deleted task', tsk.id);
	res.send({
		"status": "success"
	});
});

WsServer(http);

function exitHandler(options, err) {
    if (options.cleanup) console.log('clean');
    if (err) console.log(err.stack);
	
	for (let id in taskBus) {
		let tsk = taskBus[id]; 
		tsk.close();
	}    
    if (options.exit) process.exit();
    // if (options.kill) process.kill(process.pid, 'SIGUSR2');
}

//do something when app is closing
process.on('exit', exitHandler.bind(null,{cleanup:true, exit: true}));

//catches ctrl+c event
process.on('SIGINT', exitHandler.bind(null, {exit:true}));

// catches "kill pid" (for example: nodemon restart)
process.on('SIGUSR1', exitHandler.bind(null, {exit:true}));
// process.on('SIGUSR2', exitHandler.bind(null, {exit:true}));

//catches uncaught exceptions
process.on('uncaughtException', exitHandler.bind(null, {exit:true}));


http.listen(port, function () {
	console.log(`Example app listening on port ${port}!`)
});

function taskOpt(body) {
	let tskOpt = Object.assign({slowMo: 0}, body);

	if (typeof body.headless == 'undefined') {
		taskOpt['headless'] = true;
	}

	delete tskOpt['name'];
	return tskOpt;
}

function runTask(name, options) {
	task = Task.run(name, options);
	taskBus[task.id] = task;
	task.restart((msg) => {
		console.log('restart use ', msg.options);
		task.close();
		delete taskBus[task.id]
		task = Task.run(msg.name, msg.options);
		taskBus[task.id] = task;	
	});
	return task;
}
