module.exports = function sniffDetector() {
  const userAgent = window.navigator.userAgent;
  let _chrome;
  if (window.chrome) {
    _chrome = window.chrome;
  }
  const createElement = window.document.createElement;
  const devicePixelRatio = window.devicePixelRatio;
  const fixedUserAgent = 'Mozilla/5.0 (X11; Linux x86_64; rv:10.0) Gecko/20150101 Firefox/47.0 (Chrome)';
  const platform = 'Linux x86_64';

  window.navigator.sniffeds = {};

  window.navigator.__defineGetter__('userAgent', function() {
    window.navigator.sniffeds['userAgent'] = true;
    return fixedUserAgent;
  });

  window.navigator.__defineGetter__('platform', function() {
    window.navigator.sniffeds['platform'] = true;
    return platform;
  });  

  window.__defineGetter__('devicePixelRatio', function() {
    window.navigator.sniffeds['devicePixelRatio'] = true;
    return devicePixelRatio;
  });  

  if (window.chrome) {
    // window.__defineGetter__('chrome', function() {
    //   window.navigator.sniffeds['chrome'] = true;
    //   return _chrome;
    // });
  }

  window.navigator.__defineGetter__('plugins', function() {
    window.navigator.sniffeds['plugins'] = true;
    return platform;
  });  

  window.navigator.__defineGetter__('languages', function() {
    window.navigator.sniffeds['languages'] = true;
    return platform;
  });

  window.document.createElement = function() {
    var args = [].slice.call(arguments);
    if (args[0] == 'canvas') {
        window.navigator.sniffeds['canvas'] = true;
    }

    if (args[0] == 'img') {
        window.navigator.sniffeds['img'] = true;
    }

    return createElement.apply(window.document, args);
  }
}
