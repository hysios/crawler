const Promise = require('bluebird');
const path = require('path');
const fs = require('fs');
const EventEmitter = require('events');
const puppeteer = require('puppeteer');
const {fork} = require('child_process');
const uuidv4 = require('uuid/v4');
const net = require('net');
const {decoder} = require('./utils');

class Task extends EventEmitter {
	constructor(task, filename, options = {}) {
		super();
		// this.script = filename
		this.id = options.id || uuidv4();
		this.filename = filename
		this.task = task;
		this.status = 'running';
		this.lastError = null;
		this.__waits = {};
		this.bindEvents();
		this.bindReceived();
		this.callbacks = new Map();
		// this.result = null;
	}

	bindEvents() {
		this.task.on('close', (code, signal) => {
			this.status = 'exit';
		})

		this.task.on('error', (err) => {
			this.lastError = err;
		})

		this.task.on('message', (msg) => {
			let {pageerror, result, exit, cmd} = msg;
			if (pageerror) {
				this.status = 'exit';
				this.lastError = pageerror;
				return
			}

			if (result) {
				this.result = result;
				return
			}

			if (exit) {
				this.status = 'exit';
				this.lastError = null;
			}

			if (cmd === 'restart') {
				console.log('received restart', msg)
				if (this._restartCallback) {
					this._restartCallback.call(null, msg)
				}
			}

			if (msg.meta) {
				this.emit('meta', msg.meta);
			}
		})
	}

	bindReceived() {
		let received = require('net').createServer();

		received.on('connection', (socket) => {
			let buf = Buffer.from([]);

			socket.on('data', (data) => {
				buf = Buffer.concat([buf, data]);
			});

			socket.on('end', () => {
				try {
					let {id, result} = decoder(buf)
					if (!id) {
						return
					}
					let wait = this.__waits[id];
					if (wait && !wait.done) {
						wait.done = true
						wait.handle(result)
						delete(this.__waits[id])
					}
				} catch(err) {
					this.status = 'error';
					this.lastError = err;
					console.error(err)
				}
			})
		});

		received.on('error', (err) => {
			console.error('Received Rrror', err);
		})

		setTimeout(() => {
			received.listen(() => {
				console.log('listen', received.address().port);
			});
		}, 0);

		this.received = received
	}

	restart(handle) {
		this._restartCallback = handle;
	}

	toJSON() {
		return {
			id: this.id,
			status: this.status,
			script: this.filename,
			// title: this.getTitle(),
			lastError: this.lastError,
			result: this.result
		}
	}

	getTitle() {
		return new Promise((resolve) => {
			this.send({cmd: 'title'}, resolve);	
		});
	}

	getResult() {
		return new Promise((resolve) => {
			this.task.on('message', (msg) => {
				let {result} = msg;

				if (result) {
					this.result = result;
					resolve(result)
				}	
			})
		});
	}

	sendChat(room, text) {
		return new Promise((resolve) => {
			this.send({cmd: 'type', room: room, text: text}, resolve);
		});		
	}

	click(selector) {
		return new Promise((resolve) => {
			this.send({cmd: 'click', params: [selector]}, resolve);
		});	
	}

	type(text) {
		return new Promise((resolve) => {
			this.send({cmd: 'type', text: text}, resolve);
		});
	}

	timeout(ms) {
		return new Promise((resolve, reject) => {
    		let id = setTimeout(() => {
					clearTimeout(id);
					reject('Timed out in '+ ms + 'ms.')
			}, ms)
    	});
  	}

	getScreenshot() {
		return new Promise((resolve) => {
			this.send({cmd: 'screenshot', params: [{type: 'jpeg', quality: 10}]}, resolve);
		});
	}

	send(msg, handle, options = {received: this.received}) {
		msg.id = (new Date()).getTime()

		try {

			this._waitMsg(msg.id, (result) => {
				handle(result);
			});
			
			this.task.send(msg, options.received);	
		} catch(e) {
			console.error(e);
			this._cancelWait(msg.id);
		}
	}

	mouse(cmd, params, handle) {
		this.send({cmd: 'mouse', subcmd: cmd, params: params}, handle)	
	}

	keyboard(cmd, params, handle) {
		this.send({cmd: 'keyboard', subcmd: cmd, params: params}, handle)	
	}

	setURL(url, handle) {
		this.send({cmd: 'goto', params: [url, {waitUntil: 'networkidle'}]}, handle)
	}

	close() {
		this.task.kill('SIGHUP');
		if (this.received) {
			this.received.close();
		}
	}

	_waitMsg(id, handle) {
		this.__waits[id] = {
			done: false,
			handle: handle	
		}
	}

	_cancelWait(id) {
		delete this.__waits[id];
	}

	static run(filename, options = {}) {
		// child_process.fork(modulePath[, args][, options])
		let args = [];

		for (let k in options) {
			let value = options[k]
			if (k == 'headless' && value) {
				continue;
			}
			args.push('--' + k);
			args.push(value.toString());
		}

		args.push('-t');
		args.push(filename);

		let wrk = fork('./lib/worker.js', args);
		let taskFile = path.join(process.cwd(), 'tasks', filename, 'task.js');
		let task = new Task(wrk, filename, {id: options.id});

		(function() {
			try {
				require(taskFile)(task);
			} catch(e) {
				console.log('extend task error, ignore');
				console.error(e);
			}
		})()
		return task
	}

}

module.exports = Task;