const Promise = require('bluebird');

function encoder(id, result) {
	let buf = Buffer.from([]);
	try {
		if (Buffer.isBuffer(result)) {
			buf = Buffer.alloc(result.length + 12);
			buf.writeUIntLE(id, 0, 8);
			buf.write('buff', 8, 12);
			result.copy(buf, 12);
		} else {
			let j = JSON.stringify(result);
			buf = Buffer.alloc(Buffer.byteLength(j) + 12);
			buf.writeUIntLE(id);
			buf.write('json', 8, 12);
			buf.write(j, 12);
		}
	} catch (e) {
		//
	}

	return buf
}

function decoder(buf) {
	let id = 0, result = null;
	try {
		id = buf.readUIntLE(0, 8), 
		result = null;

		let type = buf.slice(8, 12)
		if (type.toString() === 'buff') {
			result = buf.slice(12)
		} else if (type.toString() === 'json') {
			try {
				let raw = buf.slice(12);
				result = JSON.parse(raw);
			} catch(e) {
				result = null
			}
		}
	} catch (err) {
		id = 0
		result = null
	}
	return {
		id: id,
		result: result,
	}
}


function sleep(ms) {
	new Promise((resolve) => {
		setTimeout(resolve, ms);
	});
}


module.exports = {
	encoder,
	decoder,
	sleep
}

