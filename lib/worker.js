const Promise = require('bluebird');
const puppeteer = require('puppeteer');
const path = require('path');
const fs = require('fs');
const net = require('net')
const cli = require('cli')
const {encoder} = require('./utils');
const EventEmitter = require('events');
const mkdirp = require('mkdirp');
const sniffDetector = require('./sinff');
const {Restart} = require('./status');
var debug = require('./debug')('worker')

require('console-dump');

let options = cli.parse({
  task: ['t', 'run task name in tasks dir', 'string'],
  id: [null, 'task id', 'string'],
  headless: ['l', 'Headless Mode', 'false', true],          // -l, --headless FILE   A file to process
  slowMo: [ 's', 'Slow it down', 'int', 0],                 // -t, --time TIME   An access time
  executablePath: [ 'e', 'chrome executable path', 'string'],
  startupPage: ['p', 'startup page url', 'string']
});

if (!options.task) {
  console.error('must have task argument to launch!');
  process.exit(1);
}

// inject current path
module.paths.push(process.cwd());

let taskfile = path.join('tasks', options.task),
  handle = require(taskfile);

const allowMethods = ["$","$$","addScriptTag","click","close","content","cookies","deleteCookie","emulate","emulateMedia",
  "evaluateOnNewDocument","exposeFunction","focus","frames","goBack","goForward","goto","hover","injectFile","keyboard",
  "mainFrame","mouse","pdf","plainText","press","reload","screenshot","setContent","setCookie","setExtraHTTPHeaders",
  "setJavaScriptEnabled","setRequestInterceptionEnabled","setUserAgent","setViewport","title","tracing",
  "type","url","viewport","waitFor","waitForFunction","waitForNavigation","waitForSelector"];

class Worker extends EventEmitter {
  constructor(handle, pupOpts) {
    super()
    this.options = pupOpts;
    this.postServer = null;
    this.handle = handle;
    this.startupPage = pupOpts.startupPage;
    this.userDataDir = pupOpts.userDataDir;

    if (this.userDataDir) {
      mkdirp.sync(this.userDataDir);
    }

    (async () => {
      await this.reset()  
    })();
  }

  bindEvents() {
    this.page.on('console', (...args) => {
      for (let i =0; i < args.length; ++i)
          console.log(`task ${i}: ${args[i]}`);
    });

    this.page.on('pageerror', (msg) => {
      console.log('PAGE ERROR', msg)
      process.send({pageerror: msg})
    });

    this.page.on('error', (msg) => {
      console.log('ERROR', msg)
      process.send({pageerror: msg})
    });

    this.page.on('load', async () => {
      let meta = {
        meta: {
          title: await this.page.title(),
        }
      };

      process.send(meta)
    })

    this.page.on('framenavigated', async (frame) => {
      let meta = {
        meta: {
          url: await frame.url(),
        }
      };

      process.send(meta)
    });    

    process.on('uncaughtException', (err) => {
      console.log('PROCESS ERROR')
      console.error(err)
      this.postRestart()
    })

    process.on('exit', async (code) => {
      console.log('PROCESS EXIT', code)
      await this.close()
    })  

    setTimeout(async () => {
      let page = this.page;
      console.dump('Sniffeds: ', (await page.evaluate(() => navigator.sniffeds)));
      console.dump('platform: ', (await page.evaluate(() => navigator.platform)));
    }, 5000)
  }

  async run() {
    try {
      let result = await this.handle(this.page, this)
      process.send({result: result})
    } catch(e) {
      console.log('RUN', e);
      if (typeof e === 'error') {
        console.error(e)
      } else if (e === Restart) {
        this.postRestart()
      }
    }
  }

  async reset() {
    let pupOpts = this.options;
    this.browser = await puppeteer.launch(pupOpts);
    this.page = await this.browser.newPage();
    await this.page.evaluateOnNewDocument(sniffDetector);
    this.bindEvents();
    this.processMessage();
    await this.run(); 
  }

  async newPage () {
    this.page = await this.browser.newPage()
    await this.page.evaluateOnNewDocument(sniffDetector);
    // this.bindEvents();
    // this.processMessage();
    return this.page;
  }

  taskPush(handle) {
    this.processQueue.push(handle);
  }


  postRestart() {
    debug('restart options:', this.options);
    let opt = {
      id: options.id,
      headless: this.options.headless,
      slowMo: this.options.slowMo,
      // userDataDir: this.options.userDataDir,
    }
    let restOpt = Object.assign({cmd: 'restart', name: options.task, options: opt})
    process.send(restOpt);    
  }

  processMessage() {
    process.on('message', async (m, server) => {
    
      let {id, cmd, params} = m;
      if (!cmd) {
        return;
      }
      if (server) {
        this.postServer = server;
      }

      if (!!~allowMethods.indexOf(cmd)) {
        try {
          let result = null;
          let page = this.page;
          if (cmd === 'mouse') {
            let {subcmd} = m;
            if (subcmd !== 'move') {
              console.log('mouse event', subcmd, params)
            }
            let mouse = page.mouse;
            result = await mouse[subcmd].apply(mouse, params || [])
          } else if (cmd === 'keyboard') {
            let {subcmd} = m;
            console.log('keyboard event', subcmd, params)
            result = await page.keyboard[subcmd].apply(page.keyboard, params || [])
          } else if (cmd === 'type')  {
            let {text, room} = m;
            if (room) {
              let username = await page.evaluate((room) => {
                let nicks = $(".chat_item .nickname").toArray();
                for (let i in nicks) {
                  let nick = nicks[i];

                  if ($(nick).text().trim() == room) {
                    let username = $(nick).parents(".chat_item").attr("data-username");
                    return Promise.resolve(username);
                  }
                }
                return Promise.resolve(null);
              }, room)
              if (username) {
                await page.click(`.chat_item[data-username="${username}"]`);
              }
            }

            let editarea = await page.$('#editArea'),
              lines = text.split("\n");

            await page.focus('#editArea')
            for (let i in lines) {
              let line = lines[i];
              await page.type(line)  
              await page.keyboard.down('Control');
              await page.press('Enter');
              await page.keyboard.up('Control');
            }

            await page.click(".btn_send")
            result = true;
          } else  {
            result = await page[cmd].apply(page, params || []);
          }

          this.postMessage(id, result, encoder);
        } catch(err) {
          this.postError(id, err);
        }
      }
    });
  }

  postMessage(id, msg, encoder = encoder) {
    if (this.postServer) { 
      let port = this.postServer.address().port;
      let client = net.createConnection({host: '127.0.0.1', port: port}, ()=> {
        client.end(encoder(id, msg));
      })
    }
  }

  postError(id, err) {
    if (this.postServer) { 
      let port = this.postServer.address().port;
      let client = net.createConnection({host: '127.0.0.1', port: port}, ()=> {
        client.end(JSON.stringify({id: id, error: err.message}));
      });
    }
  }  

  writeJSON(filename, object) {
    let fullname = `./data/${options.id}/${filename}`;
    debug("write to json file " + fullname);
    fs.writeFileSync(fullname, JSON.stringify(object));
  }

  readJSON(filename) {
    let fullname = `./data/${options.id}/${filename}`;
    debug("read json file " + fullname);
    return fs.readFileSync(fullname, 'utf8');
  }

  async close() {
    await browser.close()
    process.send({exit: true})    
  }
}

(async () => {

  let pupOpt = {
    headless: options.headless,
    slowMo: options.slowMo,
    // dumpio: true,
    timeout: 0,
    userDataDir: `./data/${options.id}`,
    executablePath: options.executablePath,
    startupPage: options.startupPage
    // args: ['--no-sandbox', '--disable-setuid-sandbox']
  };

  debug('worker opt:', options);
  let wrk = new Worker(handle, pupOpt);
  // wrk.run()
})();

var memwatch = require('memwatch-next');

memwatch.on('leak', function(info) {
  console.log(info)
});

