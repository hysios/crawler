const Promise = require('bluebird');
const EventEmitter = require('events');
const {sleep} = require('./utils');

class ScreenRecorder extends EventEmitter {

	constructor(socket, task) {
		super();

		this.task = task;
		this.socket = socket;
		this.bindEvents();
	}

	bindEvents() {
		this.task.on('meta', (meta) => {
			this.socket.emit('MetaChange', meta);
		})
	}

	async loop () {
		this._run = true;

		while(this._run) {
			try {
				// let proc = this.processQueue.shift();

				// if (proc) {
				// 	await Promise.race([
				//     	proc,
				//     	this.task.timeout(1500)
	   			//  ]);
				// }

				let frame = await Promise.race([
				    this.task.getScreenshot(),
				    this.task.timeout(1500)
	   		    ]);
	   		    if (frame) {
					this.socket.emit('FrameChange', frame);
				}
			} catch(e) {
				if (typeof e === 'string')  {
					console.log(e)
				} else {
					console.error(e)
				}
			}
			await sleep(100);
		}
	}

	async reload() {
		return await new Promise((resolve) => {
			this.task.send({cmd: 'reload', params: [{waitUntil: 'networkidle' }]}, resolve);
		});
	}

	async mouse(cmd, x, y, options = {}) {
		return await new Promise((resolve) => {
			this.task.mouse(cmd, [x, y, options], resolve)	
		});
	}

	meta() {
		this.task.send({cmd: 'title'}, (title) => {
			this.task.send({cmd: 'url'}, (url) => {
				let meta = {
					meta: {
						title: title,
						url: url,
					}
				}
				this.socket.emit('MetaChange', meta)
			})
		})
	}

	keyboard(msg) {
		this.task.keyboard(msg.type, msg.x, msg.y)
	}

	join() {
		this.count++;
	}

	leave() {
		this.count--;
		if (this.count == 0) {
			this.close();
		}
	}

	close() {
		console.log('close recorder');
		this._run = false;
	}
}

module.exports = ScreenRecorder;