const ScreenRecorder = require('./recorder');
let taskBus = global.taskBus;
console.log()
module.exports = function(http) {
	var io = require('socket.io')(http);
	// io.set('log level', 1);
	// io.set('transports', ['websocket']);

	io.on('connection', function(socket) {
	    socket.on('CreateSession', function(msg){
			console.log('CreateSession', msg)
	    	try {
	    		let tsk = taskBus[msg.taskId];
	        	socket.join(msg.taskId);
	        	let recorder = socket.recorder;
	        	if (recorder) {
	        		recorder.close();
	        	}
	        	recorder = new ScreenRecorder(socket, tsk);
	        	socket.taskId =  msg.taskId;
	        	socket.recorder = recorder;

	        	recorder.loop()
	    	} catch(e) {
	    		console.error(e)
	    	}
	    });
	    
	    socket.on('disconnect', function() {
        	// socket.set('taskId', msg.taskId)
        	let recorder = socket.recorder;
        	if (recorder) {
        		recorder.close()
        	}
	    })

	    socket.on('Reload', function() {
			let recorder = socket.recorder;
        	if (recorder) {
        		console.log(recorder.reload());
        	}	    	
	    });

	    socket.on('Meta', function() {
			let recorder = socket.recorder;
        	if (recorder) {
        		recorder.meta();
        	}	 
	    });

	    socket.on('SetURL', function(url) {
			let recorder = socket.recorder;
        	if (recorder) {
        		recorder.task.setURL(url);
        	}	 
	    })

	    socket.on('Mouse', function(msg) {
			let recorder = socket.recorder;
        	if (recorder) {
	    		recorder.mouse(msg.cmd, msg.x, msg.y, {button: msg.button})
	    	}
	    });

	    socket.on('Keyboard', function(msg) {
			let recorder = socket.recorder;
        	if (recorder) {	    	
	    		recorder.keyboard(msg)
	    	}
	    });

	    socket.on('PageChange', function(msg){
	        socket.join(msg);
	        io.sockets.in(msg).emit('SessionStarted', '');
	        console.log('PageChange');
	    });
	    socket.on('JoinRoom', function(msg){
	        socket.join(msg);
	        io.sockets.in(msg).emit('SessionStarted', '');
	    });
	    socket.on('ClientMousePosition', function(msg){
	        socket.broadcast.to(socket.room).emit('ClientMousePosition', {PositionLeft:msg.PositionLeft, PositionTop:msg.PositionTop});
	    });
	    socket.on('AdminMousePosition', function(msg){
	        socket.broadcast.to(msg.room).emit('AdminMousePosition', {PositionLeft:msg.PositionLeft, PositionTop:msg.PositionTop});
	    });
	    socket.on('changeHappened', function(msg){
	        socket.broadcast.to(msg.room).emit('changes', msg.change);
	    });
	    socket.on('DOMLoaded', function(msg){
	        socket.broadcast.to(msg.room).emit('DOMLoaded', '');
	    });
	});	
}